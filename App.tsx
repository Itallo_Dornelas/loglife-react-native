import React from 'react';
import Providers from './src/contexts';

import Routes from './src/routes';

const App = () => {
  return (
    <Providers>
      <Routes />
    </Providers>
  );
};

export default App;
