import React, { Dispatch, SetStateAction } from 'react';
import { ScrollView, Text, View } from 'react-native';
import ListComponent from '../../components/FlatList';
import Header from '../../components/Header';
import { styles } from './styles';

interface IAvailableCollectionsTemplate {
  height: number;
  setValueIdCollect: Dispatch<SetStateAction<string>>;
}

const AvailableCollectionsTemplate: React.FC<IAvailableCollectionsTemplate> = ({
  height,
  setValueIdCollect,
}) => {
  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <View style={styles.root}>
        <Header height={height} path={'Home'} />
        <View style={styles.container}>
          <Text style={styles.title}> Coletas Disponíveis</Text>
          <ListComponent
            path={'InfoCollections'}
            setValueIdCollect={setValueIdCollect}
          />
        </View>
      </View>
    </ScrollView>
  );
};

export default AvailableCollectionsTemplate;
