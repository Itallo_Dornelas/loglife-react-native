import React, { FC } from 'react';
import { useForm } from 'react-hook-form';
import { Image, View } from 'react-native';
import Logo from '../../../assets/Logo.png';
import Button from '../../components/Button';
import Input from '../../components/Input';
import { styles } from './styles';

interface ISignInTemplate {
  heigth: number;
  onSignInPressed: (data: any) => void;
  validatorRegExp: RegExp;
}

const SignInTemplate: FC<ISignInTemplate> = ({
  onSignInPressed,
  heigth,
  validatorRegExp,
}) => {
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm();

  return (
    <View style={styles.container}>
      <Image
        source={Logo}
        style={[styles.logo, { height: heigth * 0.3 }]}
        resizeMode="contain"
      />
      <View></View>
      <Input
        name="username"
        placeholder="Usúario"
        control={control}
        rules={{
          required: 'Campo obrigatório',
        }}
      />
      <Input
        name="email"
        keyboardType="email-address"
        placeholder="Email"
        control={control}
        rules={{
          required: 'O e-mail é obrigatório',
          pattern: {
            value: validatorRegExp,
            message: 'Deve usar um email válido',
          },
        }}
      />

      <Input
        name="password"
        placeholder="Senha"
        secureTextEntry
        control={control}
        rules={{
          required: 'Senha é obrigatória',
          minLength: {
            value: 6,
            message: 'A senha deve ter no mínimo 6 caracteres',
          },
          maxLength: {
            value: 20,
            message: 'A senha deve ter no máximo 20 caracteres',
          },
        }}
      />

      <Button
        bgColor="#304567"
        fgColor="white"
        text={'Entrar'}
        toUpperCase={true}
        onPress={handleSubmit(onSignInPressed)}
      />
    </View>
  );
};

export default SignInTemplate;
