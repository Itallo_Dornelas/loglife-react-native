import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    padding: 24,
    flex: 1,
    backgroundColor: '#75afdb',
  },

  logo: {
    width: '70%',
    maxWidth: 300,
    maxHeight: 200,
  },
});
