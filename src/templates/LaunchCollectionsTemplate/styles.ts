import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  root: {
    alignItems: 'center',
    padding: 24,
  },
  container: {
    marginTop: 20,
    alignItems: 'center',
  },
  title: {
    width: '100%',
    padding: 15,
    marginVertical: 5,
    alignItems: 'center',
    borderRadius: 10,
    borderColor: '#75afdb',
    color: '#75afdb',
    fontSize: 24,
    borderWidth: 2,
    fontWeight: 'bold',
    textAlign: 'center',
    textTransform: 'uppercase',
  },
  subtitle: {
    padding: 15,
    marginVertical: 5,
    alignItems: 'center',
    color: '#63D2FF',
    fontSize: 15,
    fontWeight: 'bold',
    textAlign: 'center',
    marginHorizontal: 10,
    textTransform: 'uppercase',
  },
  view: {
    width: '100%',
    justifyContent: 'space-evenly',
    flexDirection: 'row',
    marginHorizontal: 10,
  },
  horizontal: {
    width: '100%',
    alignItems: 'center',
  },

  button: {
    alignItems: 'center',
    width: '100%',
    marginVertical: 5,
    borderRadius: 10,
    backgroundColor: '#FFF',
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'center',
    textTransform: 'uppercase',
  },
  text: {
    color: '#63D2FF',
    fontSize: 15,
    fontWeight: 'bold',
    textAlign: 'center',
    paddingBottom: 5,
    textTransform: 'uppercase',
  },
});
