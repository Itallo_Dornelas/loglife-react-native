import React from 'react';
import { useForm } from 'react-hook-form';
import { ScrollView, Text, TouchableOpacity, View } from 'react-native';
import Ionicons from '@expo/vector-icons/Ionicons';
import { TCollect } from '../../@types';
import Button from '../../components/Button';
import Header from '../../components/Header';
import Input from '../../components/Input';
import { styles } from './styles';
import { ImagePickerResult } from 'expo-image-picker';

interface ILaunchCollectionsTemplate {
  height: number;
  valueIdFiltred: TCollect;
  onPressed: (data: any) => void;
  validatorNumberRegExp: RegExp;
  imagePickerCall: () => Promise<void>;
  image: ImagePickerResult | undefined;
}

const LaunchCollectionsTemplate: React.FC<ILaunchCollectionsTemplate> = ({
  height,
  valueIdFiltred,
  onPressed,
  validatorNumberRegExp,
  image,
  imagePickerCall,
}) => {
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm();

  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <View style={styles.root}>
        <Header height={height} path={'AvailableCollections'} />
        <View style={styles.container}>
          <Text style={styles.title}>Lançamento da Coleta</Text>
          <View style={styles.view}>
            <Text style={styles.subtitle}>
              Protocolo: {valueIdFiltred.number}{' '}
            </Text>
            <Text style={styles.subtitle}>
              Cliente: {valueIdFiltred.customerIDAddress.trading_firstname}{' '}
              {valueIdFiltred.customerIDAddress.company_lastname}
            </Text>
          </View>
          <Text style={styles.subtitle}>
            Remetente: {valueIdFiltred.remetente}{' '}
          </Text>
        </View>
        <Input
          name="responsible"
          placeholder="Responsável"
          control={control}
          rules={{
            required: 'Campo obrigatório',
          }}
        />
        <View style={styles.horizontal}>
          <Input
            name="volume"
            placeholder="Volume"
            control={control}
            rules={{
              required: 'Campo obrigatório',
              pattern: {
                value: validatorNumberRegExp,
                message: 'Somente números positivos',
              },
            }}
          />
          <Input
            name="sample"
            placeholder="Qtde Amostras"
            control={control}
            rules={{
              required: 'Campo obrigatório',
              pattern: {
                value: validatorNumberRegExp,
                message: 'Somente números positivos',
              },
            }}
          />
        </View>
        <Input
          name="note"
          placeholder="Observações"
          control={control}
          height={100}
        />
        <TouchableOpacity style={styles.button} onPress={imagePickerCall}>
          <Ionicons name="camera-outline" size={35} color="#75afdb" />
          <Text style={styles.text}>
            {image ? 'Sua imagem foi salva' : 'Declaração'}
          </Text>
        </TouchableOpacity>
        <View style={styles.view}>
          <Button
            bgColor="#FCCA46"
            fgColor="white"
            text={'Ocorrência'}
            path=""
            toUpperCase={true}
            width={'45%'}
          />
          <Button
            bgColor="#63D2FF"
            fgColor="white"
            text={'Finalizar'}
            onPress={handleSubmit(onPressed)}
            toUpperCase={true}
            width={'45%'}
          />
        </View>
      </View>
    </ScrollView>
  );
};

export default LaunchCollectionsTemplate;
