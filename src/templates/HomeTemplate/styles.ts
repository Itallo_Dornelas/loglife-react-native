import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  root: {
    alignItems: 'center',
    padding: 24,
  },
  container: {
    width: '100%',
    marginTop: 50,
  },
  title: {
    width: '100%',
    padding: 15,
    marginVertical: 5,
    alignItems: 'center',
    color: '#63D2FF',
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  header: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    padding: 8,
  },
  headerSide: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  logo: {
    width: '70%',
    borderRadius: 50,
    maxWidth: 150,
    maxHeight: 80,
  },
  qrcode: {
    marginTop: 15,
    borderRadius: 50,
    alignItems: 'center',
  },
});
