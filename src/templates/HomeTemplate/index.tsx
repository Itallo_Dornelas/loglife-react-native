import React from 'react';
import { ScrollView, View, Image, Text } from 'react-native';

import Icon from '../../../assets/iconLogLife.png';
import Button from '../../components/Button';
import Ionicons from '@expo/vector-icons/Ionicons';
import MaterialCommunityIcons from '@expo/vector-icons/MaterialCommunityIcons';
import QRCode from 'react-native-qrcode-svg';

import { styles } from './styles';
import { IValueSignIn } from '../../@types';

interface IHomeTemplate {
  height: number;
  valueSignIn: IValueSignIn;
}

const HomeTemplate: React.FC<IHomeTemplate> = ({ height, valueSignIn }) => {
  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <View style={styles.root}>
        <View style={styles.header}>
          <View style={styles.headerSide}>
            <Ionicons name="notifications-outline" size={32} color="#75afdb" />
            <MaterialCommunityIcons
              name="cog-outline"
              size={32}
              color="#75afdb"
            />
          </View>
        </View>
        <Image
          source={Icon}
          style={[styles.logo, { height: height * 0.3 }]}
          resizeMode="contain"
        />
        <View style={styles.container}>
          <Text style={styles.title}>
            Olá {valueSignIn.username}, seja bem vindo!
          </Text>
          <Button
            borderColor="#63D2FF"
            fgColor="#63D2FF"
            fontSize={24}
            borderWidth={2}
            text={'Coletas Disponíveis'}
            path={'AvailableCollections'}
            toUpperCase={true}
          />

          <View style={styles.qrcode}>
            <QRCode />
          </View>
        </View>
        <Button
          fgColor="#63D2FF"
          fontSize={15}
          text={'Sair'}
          path={'SignIn'}
          width={'20%'}
          toUpperCase={true}
        />
      </View>
    </ScrollView>
  );
};

export default HomeTemplate;
