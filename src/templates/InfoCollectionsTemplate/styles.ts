import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  root: {
    alignItems: 'center',
    padding: 8,
  },
  header: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 8,
  },
  headerSide: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  logo: {
    width: '70%',
    borderRadius: 50,
    maxWidth: 80,
    maxHeight: 40,
  },
  container: {
    width: '100%',
    marginTop: 50,
    alignItems: 'center',
  },
  title: {
    width: '100%',
    padding: 15,
    marginVertical: 5,
    alignItems: 'center',
    borderRadius: 10,
    borderColor: '#75afdb',
    color: '#75afdb',
    fontSize: 24,
    borderWidth: 2,
    fontWeight: 'bold',
    textAlign: 'center',
    textTransform: 'uppercase',
  },
});
