import React from 'react';
import { ScrollView, Text, View } from 'react-native';
import CardComponent from '../../components/Card';
import Header from '../../components/Header';
import { styles } from './styles';

interface IInfoCollectionsTemplate {
  height: number;
}

const InfoCollectionsTemplate: React.FC<IInfoCollectionsTemplate> = ({
  height,
}) => {
  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <View style={styles.root}>
        <Header height={height} path={'AvailableCollections'} />
        <View style={styles.container}>
          <Text style={styles.title}>Informações das Coletas</Text>
          <CardComponent />
        </View>
      </View>
    </ScrollView>
  );
};

export default InfoCollectionsTemplate;
