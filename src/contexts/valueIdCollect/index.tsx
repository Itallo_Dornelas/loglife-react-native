import {
  createContext,
  Dispatch,
  ReactNode,
  SetStateAction,
  useContext,
  useState,
} from 'react';
import { TCollect } from '../../@types';

interface IValueIdCollectProviderData {
  valueIdCollect: string;
  setValueIdCollect: Dispatch<SetStateAction<string>>;
  valueIdFiltred: TCollect;
  setValueIdFiltred: Dispatch<SetStateAction<TCollect>>;
}

interface ValueIdCollectProviderProps {
  children: ReactNode;
}

const ValueIdCollectContext = createContext<IValueIdCollectProviderData>(
  {} as IValueIdCollectProviderData
);

export const ValueIdCollectProvider = ({
  children,
}: ValueIdCollectProviderProps) => {
  const [valueIdCollect, setValueIdCollect] = useState(String);
  const [valueIdFiltred, setValueIdFiltred] = useState<TCollect>(
    {} as TCollect
  );

  return (
    <ValueIdCollectContext.Provider
      value={{
        setValueIdCollect,
        valueIdCollect,
        valueIdFiltred,
        setValueIdFiltred,
      }}
    >
      {children}
    </ValueIdCollectContext.Provider>
  );
};

export const useValueIdCollect = () => useContext(ValueIdCollectContext);
