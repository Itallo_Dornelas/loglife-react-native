import {
  createContext,
  Dispatch,
  ReactNode,
  SetStateAction,
  useContext,
  useState,
} from 'react';
import { IValueSignIn } from '../../@types';

interface IValueSignInProviderData {
  valueSignIn: IValueSignIn;
  setValueSignIn: Dispatch<SetStateAction<IValueSignIn>>;
}

interface ValueSignInProviderProps {
  children: ReactNode;
}

const ValueSignInContext = createContext<IValueSignInProviderData>(
  {} as IValueSignInProviderData
);

export const ValueSignInProvider = ({ children }: ValueSignInProviderProps) => {
  const [valueSignIn, setValueSignIn] = useState<IValueSignIn>(
    {} as IValueSignIn
  );

  return (
    <ValueSignInContext.Provider value={{ setValueSignIn, valueSignIn }}>
      {children}
    </ValueSignInContext.Provider>
  );
};

export const useValueSignIn = () => useContext(ValueSignInContext);
