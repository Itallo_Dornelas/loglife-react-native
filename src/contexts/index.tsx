import React, { ReactNode } from 'react';
import { ValueIdCollectProvider } from './valueIdCollect';

import { ValueSignInProvider } from './valueSignIn';

interface ProvidersProps {
  children: ReactNode;
}
const Providers = ({ children }: ProvidersProps) => {
  return (
    <ValueSignInProvider>
      <ValueIdCollectProvider>{children}</ValueIdCollectProvider>
    </ValueSignInProvider>
  );
};

export default Providers;
