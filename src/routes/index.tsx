import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import SignIn from '../pages/SignIn';
import Home from '../pages/Home';
import AvailableCollections from '../pages/AvailableCollections';
import InfoCollections from '../pages/InfoCollections';
import LaunchCollections from '../pages/LaunchCollections';

const Stack = createNativeStackNavigator();

const Navigation = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{ headerShown: false }}>
        <Stack.Screen name="SignIn" component={SignIn} />
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen
          name="AvailableCollections"
          component={AvailableCollections}
        />
        <Stack.Screen name="InfoCollections" component={InfoCollections} />
        <Stack.Screen name="LaunchCollections" component={LaunchCollections} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Navigation;
