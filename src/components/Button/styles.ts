import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    width: '100%',
    padding: 15,
    marginVertical: 5,
    alignItems: 'center',
    borderRadius: 10,
  },

  text: {
    fontWeight: 'bold',
    color: 'white',
    textAlign: 'center',
  },
});
