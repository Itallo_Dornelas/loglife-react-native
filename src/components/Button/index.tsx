import { useNavigation } from '@react-navigation/native';
import { FC } from 'react';
import { Text, Pressable } from 'react-native';
import { TNav } from '../../@types';

import { styles } from './styles';

interface IButtonProps {
  path?: string;
  text: string;
  onPress?: () => void;
  borderColor?: string;
  borderWidth?: number;
  bgColor?: string;
  fgColor?: string;
  fontSize?: number;
  toUpperCase?: boolean;
  width?: string;
}

const Button: FC<IButtonProps> = ({
  path,
  text,
  borderColor,
  bgColor,
  borderWidth,
  fgColor,
  fontSize,
  width,
  onPress,
  toUpperCase,
}) => {
  const navigation = useNavigation<TNav>();
  const onPressed = (path: string) => {
    navigation.navigate(path);
  };
  return (
    <Pressable
      onPress={path ? () => onPressed(path) : onPress}
      style={[
        styles.container,
        bgColor ? { backgroundColor: bgColor } : {},
        borderColor
          ? { borderColor: borderColor, borderWidth: borderWidth }
          : {},
        width ? { width: width } : {},
      ]}
    >
      <Text
        style={[
          styles.text,
          fgColor ? { color: fgColor } : {},
          fontSize ? { fontSize: fontSize } : {},
        ]}
      >
        {toUpperCase ? text.toUpperCase() : text}
      </Text>
    </Pressable>
  );
};

export default Button;
