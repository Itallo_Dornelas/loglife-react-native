import React, { FC } from 'react';
import { View, Text, TextInput, TextInputProps } from 'react-native';
import { Control, Controller, FieldValues } from 'react-hook-form';

import { styles } from './styles';

interface IInputProps extends TextInputProps {
  control: Control<FieldValues, any>;
  name: string;
  rules?: {};
  placeholder: string;
  width?: string;
  height?: number;
  secureTextEntry?: boolean | undefined;
}

const Input: FC<IInputProps> = ({
  control,
  name,
  rules = {},
  placeholder,
  secureTextEntry,
  width,
  height,
}) => {
  return (
    <Controller
      control={control}
      name={name}
      rules={rules}
      render={({
        field: { value, onChange, onBlur },
        fieldState: { error },
      }) => (
        <>
          <View
            style={[
              styles.container,
              { borderColor: error ? 'red' : '#e8e8e8' },
              width ? { width: width } : {},
              height ? { height: height } : {},
            ]}
          >
            <TextInput
              value={value}
              onChangeText={onChange}
              onBlur={onBlur}
              placeholder={placeholder}
              secureTextEntry={secureTextEntry}
            />
          </View>
          {error && (
            <Text style={{ color: 'red', alignSelf: 'stretch' }}>
              {error.message || 'Error'}
            </Text>
          )}
        </>
      )}
    />
  );
};

export default Input;
