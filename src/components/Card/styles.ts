import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    width: '100%',
    marginTop: 50,
    padding: 24,
    backgroundColor: '#63D2FF',
  },
  title: {
    padding: 5,
    marginVertical: 5,
    alignItems: 'center',
    color: '#ffffff',
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'left',
  },
  text: {
    textAlign: 'center',
    marginVertical: 5,
    alignItems: 'center',
    color: '#ffffff',
    fontSize: 15,
  },
  button: {
    alignItems: 'center',
    width: '100%',
    marginVertical: 5,
    padding: 15,
    borderRadius: 10,
    backgroundColor: '#FFF',
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'center',
    textTransform: 'uppercase',
  },
  text_button: {
    color: '#63D2FF',
    fontSize: 15,
    fontWeight: 'bold',
    textAlign: 'center',
    paddingBottom: 5,
    textTransform: 'uppercase',
  },
});
