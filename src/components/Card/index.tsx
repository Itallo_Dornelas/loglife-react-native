import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import { Card, Paragraph, Title } from 'react-native-paper';
import { TCollect, TNav } from '../../@types';
import { useValueIdCollect } from '../../contexts/valueIdCollect';
import { collect } from '../../utils/collect';
import { styles } from './styles';

const CardComponent: React.FC = () => {
  const { valueIdCollect, setValueIdFiltred } = useValueIdCollect();
  const navigation = useNavigation<TNav>();
  const collectById = collect.filter((item) => item.id === valueIdCollect);
  const handleCollect = (collect: TCollect) => {
    setValueIdFiltred(collect);
    navigation.navigate('LaunchCollections');
  };
  return (
    <>
      {collectById.map((collect) => {
        return (
          <Card key={collect.id} style={styles.container}>
            <Card.Content>
              <Title style={styles.title}>
                Protocolo:{' '}
                <Paragraph style={styles.text}>{collect.number}</Paragraph>
              </Title>

              <Title style={styles.title}>
                Cliente:{' '}
                <Paragraph style={styles.text}>
                  {collect.customerIDAddress.trading_firstname}{' '}
                  {collect.customerIDAddress.company_lastname}
                </Paragraph>
              </Title>
              <Title style={styles.title}>
                Endereço:
                <Paragraph style={styles.text}>
                  {' '}
                  {collect.street}, {collect.number} {collect.state}
                </Paragraph>
              </Title>
              <Title style={styles.title}>
                Ponto de referencia:{' '}
                <Paragraph style={styles.text}>
                  {collect.reference_point === ''
                    ? 'Não informado'
                    : collect.reference_point}
                </Paragraph>
              </Title>

              <Title style={styles.title}>
                Contato:{' '}
                <Paragraph style={styles.text}>{collect.remetente}</Paragraph>
              </Title>

              <Title style={styles.title}>
                Setor:{' '}
                <Paragraph style={styles.text}>
                  {collect.neighborhood}
                </Paragraph>
              </Title>
              <Card.Actions>
                <TouchableOpacity
                  style={styles.button}
                  onPress={() => handleCollect(collect)}
                >
                  <Text style={styles.text_button}>Declaração</Text>
                </TouchableOpacity>
              </Card.Actions>
            </Card.Content>
          </Card>
        );
      })}
    </>
  );
};

export default CardComponent;
