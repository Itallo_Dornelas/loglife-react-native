import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  root: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 16,
    width: '100%',
  },
  header: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 8,
  },
  headerSide: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  logo: {
    width: '70%',
    borderRadius: 50,
    maxWidth: 80,
    maxHeight: 40,
  },
});
