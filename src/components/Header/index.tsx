import React from 'react';
import { View, Image } from 'react-native';
import Ionicons from '@expo/vector-icons/Ionicons';
import Icon from '../../../assets/iconLogLife.png';

import { styles } from './styles';
import { useNavigation } from '@react-navigation/native';
import { TNav } from '../../@types';

interface IHeader {
  height: number;
  path: string;
}

const Header: React.FC<IHeader> = ({ height, path }) => {
  const navigation = useNavigation<TNav>();
  const onPressed = (path: string) => {
    navigation.navigate(path);
  };
  return (
    <View style={styles.root}>
      <View style={styles.header}>
        <View style={styles.headerSide}>
          <Ionicons
            name="arrow-back"
            size={32}
            color="#75afdb"
            onPress={() => onPressed(path)}
          />
        </View>

        <Image
          source={Icon}
          style={[styles.logo, { height: height * 0.3 }]}
          resizeMode="contain"
        />

        <View style={styles.headerSide}>
          <Ionicons name="notifications-outline" size={32} color="#75afdb" />
        </View>
      </View>
    </View>
  );
};

export default Header;
