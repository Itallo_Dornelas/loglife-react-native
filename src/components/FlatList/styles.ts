import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 50,
    borderColor: '#75afdb',
    borderWidth: 2,
    borderRadius: 5,
  },
  title: {
    fontSize: 13,
    color: '#75afdb',
    alignItems: 'center',
    fontWeight: 'bold',
    textTransform: 'uppercase',
  },
  text: {
    fontSize: 12,
    color: '#75afdb',
    alignItems: 'center',
    textAlign: 'center',
  },
  cell: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
