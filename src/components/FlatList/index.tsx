import { useNavigation } from '@react-navigation/native';
import React, { Dispatch, SetStateAction } from 'react';
import { View, Text } from 'react-native';
import { DataTable } from 'react-native-paper';
import { TNav } from '../../@types';
import { collect } from '../../utils/collect';
import { styles } from './styles';

interface IListTemplate {
  path: string;
  setValueIdCollect: Dispatch<SetStateAction<string>>;
}

const ListComponent: React.FC<IListTemplate> = ({
  path,
  setValueIdCollect,
}) => {
  const navigation = useNavigation<TNav>();
  const onPressed = (path: string, id: string) => {
    navigation.navigate(path);
    setValueIdCollect(id);
  };

  return (
    <View style={styles.container}>
      <DataTable>
        <DataTable.Header>
          <DataTable.Title style={styles.cell}>
            <Text style={styles.title}> Cliente </Text>
          </DataTable.Title>
          <DataTable.Title style={styles.cell}>
            <Text style={styles.title}>Remetente </Text>
          </DataTable.Title>
          <DataTable.Title style={styles.cell}>
            <Text style={styles.title}>Data Coleta</Text>
          </DataTable.Title>
          <DataTable.Title style={styles.cell}>
            <Text style={styles.title}>Horário</Text>
          </DataTable.Title>
        </DataTable.Header>

        {collect.map((collect) => {
          return (
            <DataTable.Row
              key={collect.id}
              onPress={() => onPressed(path, collect.id)}
            >
              <DataTable.Cell style={styles.cell}>
                <Text style={styles.text}>
                  {collect.customerIDAddress.trading_firstname}
                </Text>
              </DataTable.Cell>
              <DataTable.Cell style={styles.cell}>
                <Text style={styles.text}>{collect.remetente}</Text>
              </DataTable.Cell>
              <DataTable.Cell style={styles.cell} numeric>
                <Text style={styles.text}>{collect.collect_date}</Text>
              </DataTable.Cell>
              <DataTable.Cell style={styles.text} numeric>
                <Text style={styles.text}>13:00 AS 17:00</Text>
              </DataTable.Cell>
            </DataTable.Row>
          );
        })}
      </DataTable>
    </View>
  );
};

export default ListComponent;
