export interface IAuth {
  email: string;
  password: string;
}

export interface IValueSignIn {
  username: string;
  email: string;
  password: number;
}

export type TNav = {
  navigate: (value: string) => void;
};

export type TCollect = {
  id: string;
  situation: string;
  type: string;
  cnpj_cpf: string;
  remetente: string;
  collect_date: string;
  cep: string;
  state: string;
  street: string;
  number: string;
  neighborhood: string;
  complement: string;
  reference_point: string;
  city_id: string;
  customer_id: string;
  cityIDAddress: {
    id: string;
    situation: string;
    name: string;
    state: string;
  };
  customerIDAddress: {
    id: string;
    type: string;
    situation: string;
    trading_firstname: string;
    company_lastname: string;
    cnpj_cpf: string;
  };
};
