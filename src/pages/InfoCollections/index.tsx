import { useWindowDimensions } from 'react-native';
import InfoCollectionsTemplate from '../../templates/InfoCollectionsTemplate';

const InfoCollections: React.FC = () => {
  const { height } = useWindowDimensions();

  return <InfoCollectionsTemplate height={height} />;
};

export default InfoCollections;
