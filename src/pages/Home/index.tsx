import { useWindowDimensions } from 'react-native';
import { useValueSignIn } from '../../contexts/valueSignIn';
import HomeTemplate from '../../templates/HomeTemplate';

const Home: React.FC = () => {
  const { height } = useWindowDimensions();

  const { valueSignIn } = useValueSignIn();

  return <HomeTemplate height={height} valueSignIn={valueSignIn} />;
};

export default Home;
