import { useWindowDimensions } from 'react-native';
import { useValueIdCollect } from '../../contexts/valueIdCollect';
import AvailableCollectionsTemplate from '../../templates/AvailableCollectionsTemplate';

const AvailableCollections: React.FC = () => {
  const { height } = useWindowDimensions();

  const { setValueIdCollect } = useValueIdCollect();

  return (
    <AvailableCollectionsTemplate
      height={height}
      setValueIdCollect={setValueIdCollect}
    />
  );
};

export default AvailableCollections;
