import { useNavigation } from '@react-navigation/native';

import { useWindowDimensions } from 'react-native';
import SignInTemplate from '../../templates/SignInTemplate';
import { useValueSignIn } from '../../contexts/valueSignIn';
import { TNav } from '../../@types';

const SignIn: React.FC = () => {
  const { height } = useWindowDimensions();
  const navigation = useNavigation<TNav>();
  const validatorRegExp = new RegExp(
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  );
  const { setValueSignIn } = useValueSignIn();

  const onSignInPressed = (data: any) => {
    setValueSignIn(data);
    navigation.navigate('Home');
  };
  return (
    <SignInTemplate
      heigth={height}
      onSignInPressed={onSignInPressed}
      validatorRegExp={validatorRegExp}
    />
  );
};
export default SignIn;
