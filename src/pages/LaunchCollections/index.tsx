import { useNavigation } from '@react-navigation/native';
import { Alert, useWindowDimensions } from 'react-native';
import { TNav } from '../../@types';
import { useValueIdCollect } from '../../contexts/valueIdCollect';
import Constants from 'expo-constants';
import * as ImagePicker from 'expo-image-picker';
import LaunchCollectionsTemplate from '../../templates/LaunchCollectionsTemplate';
import { useState } from 'react';

const LaunchCollections: React.FC = () => {
  const { height } = useWindowDimensions();
  const { valueIdFiltred } = useValueIdCollect();
  const navigation = useNavigation<TNav>();
  const [image, setImage] = useState<
    ImagePicker.ImagePickerResult | undefined
  >();

  async function imagePickerCall() {
    if (Constants.platform?.ios) {
      const { status } = await ImagePicker.getMediaLibraryPermissionsAsync();

      if (status !== 'granted') {
        Alert.alert('Nós precisamos dessa permissão.');
        return;
      }
    }
    const data = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
    });
    if (data.cancelled) {
      Alert.alert('Não foi selecionada nenhuma imagem');
      setImage(undefined);
      return;
    }

    if (!data.uri) {
      Alert.alert('Não foi selecionada nenhuma imagem');
      setImage(undefined);
      return;
    }
    setImage(data);
  }
  const validatorNumberRegExp = new RegExp(/^\d+$/);
  const onPressed = () => {
    if (!image) {
      return Alert.alert('A declaração é obrigatória');
    } else {
      navigation.navigate('Home');
    }
  };

  return (
    <LaunchCollectionsTemplate
      height={height}
      onPressed={onPressed}
      valueIdFiltred={valueIdFiltred}
      validatorNumberRegExp={validatorNumberRegExp}
      imagePickerCall={imagePickerCall}
      image={image}
    />
  );
};

export default LaunchCollections;
